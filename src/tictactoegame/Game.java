// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// Game.java

package tictactoegame;




public class Game 
{  
    public Winner m_Winner;
    private int m_numberOfPlayers = 0;
    
    public GameBoard gameBoard;
    public Player player;
        
    
         
       
    // Constructor initializes game.
    
    public Game()
    {
        gameBoard = new GameBoard();
        m_Winner = new Winner();
        player = new Player();  
        initializeGame();
    }
        
    
    
    
    // Accessor to get number of players playing game.
    
    public int getNumPlayers()
    {
        return m_numberOfPlayers;
    }
    
    
    
    
    // Check for win.
    
    public Boolean checkWin()
    {
        return m_Winner.checkWin(gameBoard);        
    }
    
    
    
    
    // Clear previous game to initialize game.
    
    public void initializeGame()
    {        
        gameBoard.clearBoard();
        m_Winner.clearWin();
    }
    
        
    
    
    // Set number of players.
    
    public void numberOfPlayers(int numPlayers)
    {
        m_numberOfPlayers = numPlayers;
        
        if (m_numberOfPlayers == 1)
        {
            player.computerPlayer(true);
        }
    }
         
   
}

