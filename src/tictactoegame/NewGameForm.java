// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// NewGameForm.java

package tictactoegame;



public class NewGameForm extends javax.swing.JFrame 
{

    private Game m_game;
    private static final int COMPUTER = Player.COMPUTER;
    private static final int PLAYER_ONE = Player.PLAYER_ONE;
    private static final int PLAYER_TWO = Player.PLAYER_TWO;
    
    
    
     
    // On new game, initialize components and set new game.
    
    public NewGameForm() 
    {
        initComponents();
        
        m_game = new Game();
        m_game.initializeGame();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        singlePlayerButton = new javax.swing.JRadioButton();
        twoPlayerButton = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        playerRadioButton = new javax.swing.JRadioButton();
        computerRadioButton = new javax.swing.JRadioButton();
        submitButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(0, 153, 51));

        jLabel2.setText("Initialize Game:");

        jLabel3.setText("Number of Players:");

        buttonGroup1.add(singlePlayerButton);
        singlePlayerButton.setSelected(true);
        singlePlayerButton.setText("Single-Player");

        buttonGroup1.add(twoPlayerButton);
        twoPlayerButton.setText("Two-Player");

        jLabel4.setText("______________________________________");

        jLabel5.setText("For Single-Player Game:");

        jLabel6.setText("First Player to Move:");

        buttonGroup2.add(playerRadioButton);
        playerRadioButton.setText("Player One");
        playerRadioButton.setToolTipText("");

        buttonGroup2.add(computerRadioButton);
        computerRadioButton.setSelected(true);
        computerRadioButton.setText("Computer");
        computerRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                computerRadioButtonActionPerformed(evt);
            }
        });

        submitButton.setText("Begin Game");
        submitButton.setToolTipText("");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(twoPlayerButton)
                                            .addComponent(singlePlayerButton)))))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel4))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(playerRadioButton)
                                            .addComponent(computerRadioButton))))))
                        .addGap(0, 3, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(submitButton)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel2)
                .addGap(27, 27, 27)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(singlePlayerButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(twoPlayerButton)
                .addGap(41, 41, 41)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addGap(28, 28, 28)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(computerRadioButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(playerRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addComponent(submitButton)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    
    // Assign settings, first player to game on submit click.
    
    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        
                
        if (singlePlayerButton.isSelected() == true)
        {
            m_game.numberOfPlayers(1);
            
            
            if (computerRadioButton.isSelected() == true)
                m_game.player.setFirstPlayer(COMPUTER);
                
            
            else if (playerRadioButton.isSelected() == true)
                m_game.player.setFirstPlayer(PLAYER_ONE);
        }
        
                
        else if (twoPlayerButton.isSelected() == true)
            m_game.numberOfPlayers(2);
        
        
        this.setVisible(false);        
        
        
        // If computer player is first, show random first move.
        
        if (m_game.player.computerPlayer() == true && 
                        m_game.player.firstPlayer() == COMPUTER)
        {            
            java.awt.EventQueue.invokeLater(new Runnable() 
            {
                public void run() 
                {
                    int randomNum = m_game.player.computer().firstMove();
                            
                    new TicTacToeForm(m_game, randomNum).setVisible(true);
                }
            });
        }
        
        
        // If player's move, show board.
        
        else
            java.awt.EventQueue.invokeLater(new Runnable() 
            {
                public void run() 
                {                
                    new TicTacToeForm(m_game).setVisible(true);
                }
            });        
    }//GEN-LAST:event_submitButtonActionPerformed

    private void computerRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_computerRadioButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_computerRadioButtonActionPerformed

    
    
    
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JRadioButton computerRadioButton;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButton playerRadioButton;
    private javax.swing.JRadioButton singlePlayerButton;
    private javax.swing.JButton submitButton;
    private javax.swing.JRadioButton twoPlayerButton;
    // End of variables declaration//GEN-END:variables
}
