// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// Winner.java

package tictactoegame;




public class Winner
{
    public GameBoard m_gameBoard;
    private static final int NUM_ROWS = 3;
    private static final int NUM_COLS = 3;
    private Boolean m_win = false;
    private int m_winner = 0;
    
    
    
    
    // Accessor.
    
    public int winner()
    {
        return m_winner;
    }
    
    
    
    
    // Resets game to no winner.
    
    public void clearWin()
    {
        m_winner = 0;
        m_win = false;
    }
    
    
    
    
    // Copy gameboard to member variable gameboard to check for win.
    
    public void setGameBoard(GameBoard gameBoard)
    {
        m_gameBoard = new GameBoard();
        
        for(int idx = 0; idx < gameBoard.getBoard().length; idx++)
        {
            for(int idx2 = 0; idx2 < gameBoard.getBoard()[idx].length; idx2++)
            {
                int player = gameBoard.getBoard()[idx][idx2];
                m_gameBoard.setOccupied(idx, idx2, player);
            }
        }
    }
    
    
    
    
    // Check if a winner exists
    
    public Boolean checkWin()
    {
        checkRowWin();
        checkColumnWin();
        checkDiagonalWin();      
        
        if (m_win == true)
        {           
            return true;
        }
        
        return false;
    }
        
    
    
    
    // Copies board to member varaible gameboard, 
        // then returns whether winner exists 
    
    public Boolean checkWin(GameBoard gameBoard)
    {
        setGameBoard(gameBoard);
        
        return checkWin();
    }
    
        
    
    
    // Checks gameboard for a row winner
    
    private void checkRowWin()
    {     
        for (int playerIdx = 1; playerIdx < 4; playerIdx++)
        {
            for (int rowIdx = 0; rowIdx < NUM_ROWS; rowIdx++)
            {
                if (m_gameBoard.isOccupiedBy(rowIdx,0, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(rowIdx,1, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(rowIdx,2, playerIdx) == true)
                {
                    m_win = true;            
                    m_winner = playerIdx;
                }
            }  
        }
        
    }
    
    
    
    
    // Checks gameboard for a column winner
    
    private void checkColumnWin()
    {
        for (int playerIdx = 1; playerIdx < 4; playerIdx++)
        {
            for (int colIdx = 0; colIdx < NUM_COLS; colIdx++)
            {
                if (m_gameBoard.isOccupiedBy(0, colIdx, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(1, colIdx, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(2, colIdx, playerIdx) == true)
                {
                    m_win = true;
                    m_winner = playerIdx;
                }
            }
        }
    }
    
    
    
    
    // Checks gameboard for a diagonal winner
    
    private void checkDiagonalWin()
    {
        for (int playerIdx = 1; playerIdx < 4; playerIdx++)
        {
            for (int idx = 0, idx2 = 2; idx < NUM_COLS; idx += 2, idx2 -= 2)
            {
                if (m_gameBoard.isOccupiedBy(0, idx, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(1, 1, playerIdx) == true &&
                    m_gameBoard.isOccupiedBy(2, idx2, playerIdx) == true)
                {
                    m_win = true;
                    m_winner = playerIdx;
                }
            }
        }
    }
}
