// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// GameBoard.java

package tictactoegame;



public class GameBoard 
{
    public static final int NUM_ROWS = 3;
    public static final int NUM_COLS = 3;
    private static int[][] m_board = new int[NUM_ROWS][NUM_COLS];    
    private static Boolean m_occupied = false;
    
    
        
    
    // Accessor to get gameboard.
    
    public int[][] getBoard()
    {
        return m_board;
    }
    
    
    
    // Clear board by resetting board to zeros.
    
    public void clearBoard()
    {
        for (int rowIdx = 0; rowIdx < 3; rowIdx++)
        {
            for (int colIdx = 0; colIdx < 3; colIdx++)
            {
                m_board[rowIdx][colIdx] = 0;
            }
        }
    }
    
    
    
    
    // Compare gameboards.
    
    public Boolean equals(int[][] gameBoard)
    {
        for (int rowIdx = 0; rowIdx < NUM_ROWS; rowIdx++)
            for (int colIdx = 0; colIdx < NUM_COLS; colIdx++)
            {
                if (m_board[rowIdx][colIdx] != gameBoard[rowIdx][colIdx])
                    return false;
            }
        
        return true;
    }
    
    
    
    
    // Set space as occupied by assigning player to gameboard array.
    
    public void setOccupied(int row, int col, int player)
    {
        m_board[row][col] = player;
    }
    
    
    
    
    // Determines whether position is occupied.
    
    public Boolean isOccupied(int row, int col)
    {
        if (m_board[row][col] == 0)
            return false;
        
        else
            return true;
    }
    
    
    
    
    // Determines if provided space is occupied by specific player.
    
    public Boolean isOccupiedBy(int row, int col, int player)
    {
        if (m_board[row][col] == player)
            return true;
        
        else
            return false;
    }
    
    
    
    
    // Determines whether gameboard is full.
    
    public Boolean isFull()
    {
        for(int rowIdx = 0; rowIdx < NUM_ROWS; rowIdx++)
            for (int colIdx = 0; colIdx < NUM_COLS; colIdx++)
            {
                if (m_board[rowIdx][colIdx] == 0)
                    return false;
            }
        
        return true;
    }   
    
}
