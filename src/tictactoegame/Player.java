// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// Player.java

package tictactoegame;


public class Player 
{
    private int m_playerTurn = 1;
    private int m_firstPlayer = 0;
    private Boolean m_computerPlayer = false;
    private Computer m_computer;
    
    public static final int COMPUTER = 3;
    public static final int PLAYER_ONE = 1;
    public static final int PLAYER_TWO = 2;
    
    
    
    
    // Accessors
    
    public Computer computer() { return m_computer; }
    public int firstPlayer() { return m_firstPlayer; }
    public Boolean computerPlayer() { return m_computerPlayer; }
    public int playerTurn() { return m_playerTurn; }
    
    
    
    
    // Assign first player.
    // Set computer player true if first player.
    
    public void setFirstPlayer(int firstPlayer)
    {
        m_firstPlayer = firstPlayer;
        m_playerTurn = firstPlayer;
        
        if (firstPlayer == COMPUTER)
            computerPlayer(true);
    }
    
    
    
    
    // Set whether computer is playing.
    
    public void computerPlayer(Boolean bool)
    {
        m_computerPlayer = bool;
        m_computer = new Computer(m_playerTurn);
      
    }    
    
    
    
    
    // Set other player's turn.
    
    public void switchPlayer()
    {
        if (m_computerPlayer == false)
        {
            if (m_playerTurn == PLAYER_ONE)
                m_playerTurn = PLAYER_TWO;
            
            else
                m_playerTurn = PLAYER_ONE;
        }
        
        else
        {
            if (m_playerTurn == PLAYER_ONE)
                m_playerTurn = COMPUTER;
                        
            else
                m_playerTurn = PLAYER_ONE;
        }
    }
    
}
