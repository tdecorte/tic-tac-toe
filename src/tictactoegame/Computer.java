// Written by: Tracey DeCorte
// CST223
// Concepts of Programming Languages
// Computer.java

package tictactoegame;

import java.util.Random;


public class Computer
{
    private static int m_move = 0; 
    private int m_firstMove = 0;
    private int m_middleOuter = 0;
    private int m_cornerAdjacent = 0;
    
    private static final int NUM_ROWS = GameBoard.NUM_ROWS;
    private static final int NUM_COLS = GameBoard.NUM_COLS;
    private static final int COMPUTER = Player.COMPUTER;
    private static final int PLAYER_ONE = Player.PLAYER_ONE;
    
    
    
    
    // Constructor determines which player moves first.
    // If computer moves first, random position is generated.
    
    public Computer(int firstPlayer)
    {
        m_move = 0;
        
        if (firstPlayer == COMPUTER)
            m_firstMove = randomNum(); 
    }

    
    
    
    // Accessor returns first move.
    
    public int firstMove()
    {
        return m_firstMove;
    }
    
    
    
    
    // Generates random number indicitive of position on board.
    
    private int randomNum()
    {
        Random randomNum = new Random();
        
        return randomNum.nextInt(9);
    }
    
    
    
    
    // Given a set of possible positions, random number is generated
    // until condition is met.
    
    private int randPossValues(int num1, int num2, int num3, int num4)
    {
        int num = 0;
        
        do
        {
            num = randomNum();                
        } while (num != num1 && num != num2 && num != num3 && num != num4);

        return num;
    }
    
    
    
    
    // Search board to find winning position. If exists, return winning position
    
    private int checkWinningPosition(GameBoard gameBoard, Winner winner, int row, int col, int player)
    {
        for (int rowIdx = 0; rowIdx < NUM_ROWS; rowIdx++)
        {
            for (int colIdx = 0; colIdx < NUM_COLS; colIdx++)
            {
                if (gameBoard.isOccupied(rowIdx, colIdx) == false)
                {
                    winner.setGameBoard(gameBoard);
                    winner.m_gameBoard.setOccupied(rowIdx, colIdx, player);

                    if (winner.checkWin() == true)
                    {
                        row = rowIdx;
                        col = colIdx;
                        winner.m_gameBoard.setOccupied(rowIdx, colIdx, 0);
                        return (NUM_COLS * row) + col ;                        
                    }
                    
                    winner.m_gameBoard.setOccupied(rowIdx, colIdx, 0);

                }
            }
        }
        
        return (NUM_ROWS * NUM_COLS);
    }
    
    
    
    
    
    // If computer's move, check for winning position.
    // If no winning move possible, check for opposition's winning move
    // and block if exists.
    // If no winning positions, return optimal position.
    
    public int computerMove(GameBoard gameBoard)
    {
        int row = 0;
        int col = 0;  
        int position = 0;
            
        if (gameBoard.isOccupied(1,1) == false)
            return 4;
        
        
        
        // Check for winning position. If exists, return position.
        
        Winner winner = new Winner();
        
        
        int winPosition = checkWinningPosition(gameBoard, winner, row, col, COMPUTER);
        
        if(winPosition < (NUM_ROWS * NUM_COLS))
            return winPosition;
        
        
        
        
        // Check for opponent winning position. If exists, return position.
        
        winPosition = checkWinningPosition(gameBoard, winner, row, col, PLAYER_ONE);
        
        if (winPosition < (NUM_ROWS * NUM_COLS))
            return winPosition;
        
        
        if (gameBoard.equals(new int[][] {{0,0,0},{0,PLAYER_ONE,0},{0,0,0}}))
            return randPossValues(0,2,6,8);
        
        if (gameBoard.equals(new int[][] {{COMPUTER,0,0},{0,PLAYER_ONE,0},{0,0,0}}))
            return randPossValues(0,3,7,8);
        
        if (gameBoard.equals(new int[][] {{0,0,COMPUTER},{0,PLAYER_ONE,0},{0,0,0}}))
            return randPossValues(0,1,5,8);
        
        if (gameBoard.equals(new int[][] {{0,0,0},{0,PLAYER_ONE,0},{COMPUTER,0,0}}))
            return randPossValues(0,3,7,8);
        
        if (gameBoard.equals(new int[][] {{0,0,0},{0,PLAYER_ONE,0},{0,0,COMPUTER}}))
            return randPossValues(2,5,6,7);
        
        
        
        if (gameBoard.equals(new int[][] {{0,0,0},{0,COMPUTER,PLAYER_ONE},{0,PLAYER_ONE,0}}))
            return 8;
        
        if (gameBoard.equals(new int[][] {{0,0,0},{PLAYER_ONE,COMPUTER,0},{0,PLAYER_ONE,0}}))
            return 6;
        
        if (gameBoard.equals(new int[][] {{0,PLAYER_ONE,0},{PLAYER_ONE,COMPUTER,0},{0,0,0}}))
            return 0;
        
        if (gameBoard.equals(new int[][] {{0,PLAYER_ONE,0},{0,COMPUTER,PLAYER_ONE},{0,0,0}}))
            return 2;
        
        
        
        
        // Search for optimal position. If optimal position found, return position.
        
        if (gameBoard.equals(new int[][] {{0,0,PLAYER_ONE},{0,PLAYER_ONE,0},{COMPUTER,0,0}}))
            return 0;
                
        if (gameBoard.equals(new int[][] {{PLAYER_ONE,0,0},{0,PLAYER_ONE,0},{0,0,COMPUTER}}))
            return 2;

        if (gameBoard.equals(new int[][] {{0,0,COMPUTER},{0,PLAYER_ONE,0},{PLAYER_ONE,0,0}}))
            return 8;

        if (gameBoard.equals(new int[][] {{COMPUTER,0,0},{0,PLAYER_ONE,0},{0,0,PLAYER_ONE}}))
            return 6;
                
       
        
        
        for (int rowIdx = 0; rowIdx < NUM_ROWS; rowIdx++)
            for (int colIdx = 0; colIdx < NUM_COLS; colIdx++)
        {
            if (gameBoard.isOccupied(rowIdx, colIdx) == false)
            {
                return (rowIdx * NUM_COLS) + colIdx;
            }
        }
        
        return position;
    }
    

}